<% local viewtable, viewlibrary, pageinfo, session = ...
   html=require("acf.html") %>
Status: 200 OK
Content-Type: text/html
<% if (session.id) then
	io.write( html.cookie.set("sessionid", session.id) )
  else
	io.write (html.cookie.unset("sessionid"))
  end
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<%
local hostname = ""
if viewlibrary and viewlibrary.dispatch_component then
	local result = viewlibrary.dispatch_component("alpine-baselayout/hostname/read", nil, true)
	if result and result.value then
		hostname = result.value
	end
end
%>
<title><%= html.html_escape(hostname .. " - " .. pageinfo.controller .. "->" .. pageinfo.action) %></title>

<link rel="stylesheet" href="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.skin.."/") %>themes/jquery.mobile.icons.min.css" />
<link rel="stylesheet" href="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.skin.."/") %>themes/jqmobile-skin.min.css" />
<link rel="stylesheet" href="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.staticdir) %>/jqmobile/jquery.mobile.structure.css" />
<link rel="stylesheet" href="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.skin.."/") %>jqmobile.css"/>

<script type="text/javascript" src="<%= html.html_escape(pageinfo.wwwprefix) %>/js/jquery-latest.js"></script>
<script type="text/javascript" src="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.skin.."/") %>jqmobile.js"></script>
<script type="text/javascript" src="<%= html.html_escape(pageinfo.wwwprefix) %>/js/jquery.mobile.js"></script>

</head>
<body>
<div data-role="page" class="type-home" data-dom-cache="false" data-theme="c">

<div data-role="content">

        <% if pageinfo.controller == 'welcome' then %>
	<div id="header" class="content-secondary">
		<div id="acf-homeheader">
			<h1><img src="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.skin.."/") %>images/alogo.png" alt="Alpine logo" /></h1>
			<p>Alpine Linux</p>
		</div>
		<p class="intro"><strong>Welcome.</strong> Alpine Configuration Framework</p>
		<ul data-role="listview" data-inset="true" data-theme="c" data-divider-theme="f">
			<li data-role="list-divider">
			<span><%= html.html_escape(hostname or "unknown hostname") %></span></li>
			<li>
		<% local ctlr = pageinfo.script .. "/acf-util/logon/"

		if session.userinfo and session.userinfo.userid then
		   io.write ( string.format("\t\t\t\t\t\t<a href=\"%s\">Log off as '" .. html.html_escape(session.userinfo.userid) .. "'</a>\n", html.html_escape(ctlr) .. "logoff" ) )
		else
		   io.write ( string.format("\t\t\t\t\t\t<a href=\"%s\">Log in</a>\n", html.html_escape(ctlr) .. "logon" ) )
		end %>
			</li>
			<li><a href="/cgi-bin/acf<%= html.html_escape(pageinfo.wwwprefix) %>/">Home</a></li>
			<li><a href="http://www.alpinelinux.org">About</a></li>
		</ul>
        <p class="acf-version"></p>
	</div>	<!-- header -->
	<% end %>

        <div id="content" class="content-primary">
                <% if session.userinfo and session.userinfo.userid then %>
		<nav><div id="nav">
			<%
			local class
			local tabs
			io.write ('<ul data-role="listview" data-inset="true" data-theme="c" data-divider-theme="b" data-filter="true">')
			for x,cat in ipairs(session.menu.cats) do
				io.write (string.format("\n\t\t\t\t<li data-role='list-divider'>%s</li>\n",
					html.html_escape(cat.name)))	--start row
				for y,group in ipairs(cat.groups) do
					class=""
					if not tabs and group.controllers[pageinfo.prefix .. pageinfo.controller] then
						class="class='selected'"
						tabs = group.tabs
					end
					io.write (string.format("\t\t\t\t<li %s><a %s href=\"%s%s%s/%s\">%s</a></li>\n",
						class,class,html.html_escape(pageinfo.script),
						html.html_escape(group.tabs[1].prefix), html.html_escape(group.tabs[1].controller),
						html.html_escape(group.tabs[1].action), html.html_escape(group.name) ))
				end
			end
				io.write ( "\n\t\t\t</ul>\n")
			%>
		</div></nav><!-- nav -->
                <% end %>

                <% if not (session.userinfo and session.userinfo.userid) then %>
		<% pageinfo.viewfunc(viewtable, viewlibrary, pageinfo, session) %>
                <% end %>
        </div> <!-- content -->

</div>

<div id="footer" data-role="footer" class="footer-docs" data-theme="c">
	<p>Page generated in <%= html.html_escape(os.clock()) %> seconds on <%= html.html_escape(os.date()) %>.</p>
	<p>&copy; 2012 Alpine Linux</p>
</div> <!-- footer -->

</div> <!-- page -->

<script type="text/javascript">
	$(function(){
		$(":input:not(:submit):enabled:not([readonly]):first").focus();
	});</script>

</body>
</html>
