APP_NAME=skins-jqmobile
PACKAGE=acf-$(APP_NAME)
VERSION=0.0.1

WWW_DIST=\
	images/* \
	jqmobile.css \
	jqmobile.js \
	template-html.lsp \
	template-welcome-read-html.lsp \
	themes/jqmobile-skin.min.css \
	themes/jquery.mobile.icons.min.css \
	themes/images/ajax-loader.gif \
	themes/images/icons-png/* \

EXTRA_DIST=README Makefile config.mk

DISTFILES=$(WWW_DIST) $(EXTRA_DIST)

TAR=tar

P=$(PACKAGE)-$(VERSION)
tarball=$(P).tar.bz2
install_dir=$(DESTDIR)/$(wwwdir)/skins/jqmobile

all:
clean:
	rm -rf $(tarball) $(P)

dist: $(tarball)

install:
	mkdir -p $(install_dir)
	for i in $(WWW_DIST); do\
		dest=`dirname "$(install_dir)/$$i"`;\
		mkdir -p "$$dest";\
		cp "$$i" "$$dest";\
	done

$(tarball):	$(DISTFILES)
	rm -rf $(P)
	mkdir -p $(P)
	for i in $(DISTFILES); do\
		dest=`dirname "$(P)/$$i"`;\
		mkdir -p "$$dest";\
		cp "$$i" "$$dest";\
	done
	$(TAR) -jcf $@ $(P)
	rm -rf $(P)

# target that creates a tar package, unpacks is and install from package
dist-install: $(tarball)
	$(TAR) -jxf $(tarball)
	$(MAKE) -C $(P) install DESTDIR=$(DESTDIR)
	rm -rf $(P)

include config.mk

.PHONY: all clean dist install dist-install
