/*
 * needs cleaning
*/

//collapse page navs after use
/*
$(function(){
    $('body').delegate('.content-secondary .ui-collapsible-content', 'click',  function(){
        $(this).trigger("collapse");
    });
});
*/

// Listen for any attempts to call changePage().
$(document).bind( "pagebeforechange", function( e, data ) {

      var commandresult = $( "#content .command-results" );
      var descr =  $( "#content p.descr" );
      var error =  $( "#content p.error" );
      var commandpage = $( "#infopage .content" );
      var showinfopage = false;

      if(commandresult.length > 0) {
	commandresult.hide();
	commandpage.html( commandresult.html() );
	commandresult.remove();
	var showinfopage = true;
      }

      if(descr.length > 0) {
	descr.hide();
	commandpage.html( descr.html() );
	descr.remove();
	var showinfopage = true;
      }

      if(error.length > 0) {
	error.hide();
	commandpage.html( error.html() );
	error.remove();
	var showinfopage = true;
      }

      if(showinfopage) {
	e.preventDefault();
	$.mobile.changePage( "#infopage", {
	  transition: "pop"
	});
      }
      var section4 = $( "div.section4" );
      section4.attr({
	'data-role': 'collapsible',
	'data-mini': 'true',
	'data-collapsed': 'false',
	'data-content-theme': 'c'
      });
      var alink = $( "dd a" );
      alink.attr({
	'data-role': 'button',
	'data-mini': 'true',
	'data-theme': 'c',
	'data-inline': 'true',
	'data-iconpos': 'left',
	'data-icon': 'gear'
      });
      var inputsubmit = $( "#page input[type='submit']" );
      inputsubmit.attr({
	'data-role': 'button',
	'data-mini': 'false',
	//'data-iconpos': 'left',
	'data-theme': 'd',
	'data-inline': 'false'
      });
      var tdalink = $( "td .a" );
      tdalink.attr({
	'data-role': 'button',
	'data-mini': 'true',
	//'data-iconpos': 'left',
	'data-theme': 'c',
	'data-inline': 'true'
      });


});


// display the version of ACF
$(document).bind( 'pagecreate', function() {
    var version = $.mobile.version || "dev",
        words = version.split( "-" ),
        ver = words[0],
        //str = (words[1] || "Final"),
                str = (words[1] || ""),
        //html = 'Acf - ' + ver;
                html = 'Acf - 0.16.0';

    if( str.indexOf( "rc" ) == -1 ){
        str = str.charAt( 0 ).toUpperCase() + str.slice( 1 );
    } else {
        str = str.toUpperCase().replace(".", "");
    }

    if ( $.mobile.version && str ) {
        html += " <b>" + str + "</b>";
    }

    $( "p.acf-version" ).html( html );

    $.mobile.ajaxEnabled = false;

});



// Turn off AJAX for local file browsing
if ( location.protocol.substr(0,4)  === 'file' ||
     location.protocol.substr(0,11) === '*-extension' ||
     location.protocol.substr(0,6)  === 'widget' ) {

  // Start with links with only the trailing slash and that aren't external links
  var fixLinks = function() {
    $( "a[href$='/'], a[href='.'], a[href='..']" ).not( "[rel='external']" ).each( function() {
      this.href = $( this ).attr( "href" ).replace( /\/$/, "" ) + "/index.html";
    });
  };

  // fix the links for the initial page
  $(fixLinks);

  // fix the links for subsequent ajax page loads
  $(document).bind( 'pagecreate', fixLinks );

  // Check to see if ajax can be used. This does a quick ajax request and blocks the page until its done
  $.ajax({
    url: '.',
    async: false,
    isLocal: true
  }).error(function() {
    // Ajax doesn't work so turn it off
    $( document ).bind( "mobileinit", function() {
      $.mobile.ajaxEnabled = false;

      var message = $( '<div>' , {
        'class': "ui-footer ui-bar-e",
        style: "overflow: auto; padding:10px 15px;",
        'data-ajax-warning': true
      });
      message
        .append( "<h3>Note: Navigation may not work if viewed locally</h3>" )
        .append( "<p>The AJAX-based navigation used throughout the ACF jqMobile skin may need to be viewed on a web server to work in certain browsers. If you see an error message when you click a link, try a different browser.</p>" );
      $( document ).bind( "pagecreate", function( event ) {
        $( event.target ).append( message );
      });

    });
  });
}
