<% local viewtable, viewlibrary, pageinfo, session = ...
   html=require("acf.html")
   posix = require("posix")
%>
Status: 200 OK
Content-Type: text/html
<% if (session.id) then
	io.write( html.cookie.set("sessionid", session.id) )
  else
	io.write (html.cookie.unset("sessionid"))
  end
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<%
local hostname = ""
if viewlibrary and viewlibrary.dispatch_component then
	local result = viewlibrary.dispatch_component("alpine-baselayout/hostname/read", nil, true)
	if result and result.value then
		hostname = result.value
	end
end

local class
local tabs
for x,cat in ipairs(session.menu.cats) do
  for y,group in ipairs(cat.groups) do
    class=""
    if not tabs and group.controllers[pageinfo.prefix .. pageinfo.controller] then
      class="class='selected'"
      tabs = group.tabs
      curcat = html.html_escape(cat.name)
      curgroup = html.html_escape(group.name)
    end
  end
end
%>
<title><%= html.html_escape(hostname .. " - " .. pageinfo.controller .. "->" .. pageinfo.action) %></title>

<link rel="stylesheet" href="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.skin.."/") %>themes/jquery.mobile.icons.min.css" />
<link rel="stylesheet" href="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.skin.."/") %>themes/jqmobile-skin.min.css" />
<link rel="stylesheet" href="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.staticdir) %>/jqmobile/jquery.mobile.structure.css" />
<link rel="stylesheet" href="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.skin.."/") %>jqmobile.css"/>

<script type="text/javascript" src="<%= html.html_escape(pageinfo.wwwprefix) %>/js/jquery-latest.js"></script>
<script type="text/javascript" src="<%= html.html_escape(pageinfo.wwwprefix..pageinfo.skin.."/") %>jqmobile.js"></script>
<script type="text/javascript" src="<%= html.html_escape(pageinfo.wwwprefix) %>/js/jquery.mobile.js"></script>

</head>
<body>
<div id="page" data-role="page" class="type-index" data-dom-cache="false" data-theme="c">

<div id="header" data-role="header" data-theme="f">
  <h1><%= html.html_escape(pageinfo.controller) %></h1>
  <a href="<%= html.html_escape(pageinfo.script) %>" data-icon="home" data-iconpos="notext" data-direction="reverse">Home</a>
  <a href="#" data-icon="search" data-iconpos="notext" data-rel="dialog" data-transition="fade">Search</a>
</div><!-- /header -->

<!-- main -->
<div id="main" data-role="content">
<% if session.userinfo and session.userinfo.userid then %>
<h2><%= curcat %> > <%= curgroup %> </h2>
<% else %>
<h2><i>Session expired, please login</i>... </h2>
<% end %>
  <a id="top"></a>
  <!-- subnav -->
  <div id="subnav">
    <ul data-role="controlgroup" data-type="horizontal" class="localnav">
    <% local class="" %>
    <% for x,tab in pairs(tabs or {})  do
    if tab.prefix == pageinfo.prefix and tab.controller == pageinfo.controller and tab.action == pageinfo.action then
      class="class='ui-btn-active'"
    else
      class=""
    end
        io.write (string.format('<li><a %s href="%s%s%s/%s" data-role="button" data-transition="fade">%s</a></li>\n',
            class,html.html_escape(pageinfo.script),html.html_escape(tab.prefix),
            html.html_escape(tab.controller),html.html_escape(tab.action),
            html.html_escape(tab.name) ))
    end
    %>
    </ul>
  </div>
  <!-- /subnav -->

  <!-- content -->
  <div id="content" class="flexcols flexcols-1-4" data-mini="true" data-role="collapsible" data-collapsed="false" data-theme="e" data-content-theme="c">
    <% pageinfo.viewfunc(viewtable, viewlibrary, pageinfo, session) %>
  </div>
  <!-- /content -->

</div>
<!-- /main -->

<!-- footer -->
<div id="footer" data-role="footer" class="footer-docs" data-theme="c">
  <p>Page generated in <%= html.html_escape(os.clock()) %> seconds on <%= html.html_escape(os.date()) %>.</p>
  <p>&copy; 2012 Alpine Linux</p>
</div>
<!-- /footer -->

</div><!-- page -->

<%
if pageinfo.viewfile then
  fs=require("acf.fs")
  local sf = posix.dirname(pageinfo.viewfile) .. '/' .. pageinfo.controller .. '-' .. pageinfo.action ..'-popups-html.lsp'
  if fs.is_file(sf) then
%><!-- multipage -->
<%
    local func = haserl.loadfile(sf)
    func (viewtable, viewlibrary, pageinfo, session)
%><!-- /multipage -->
<% end %>
<% end %>

<!-- multipage -->
<div data-role="page" id="infopage">
    <div data-role="header" data-theme="e"><h2>Information</h2></div>
    <div data-role="content" data-theme="d" class="content"></div>
    <div data-role="footer" class="ui-bar">
    <a href="<%= pageurl %>#page" data-rel="back" data-role="button" data-icon="check">Close</a>&nbsp;
    <a href="<%= html.html_escape(pageinfo.script) %>" data-direction="reverse" data-role="button" data-icon="home">Home</a>
    </div>
</div>
<!-- /multipage -->



<script type="text/javascript">
	$(function(){
		$(":input:not(:submit):enabled:not([readonly]):first").focus();
	});</script>

</body>
</html>
